﻿using BerlinClockImplementation;
using BerlinClockImplementation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BerlinClock
{
    public class TimeConverter : ITimeConverter
    {
        public string convertTime(string aTime)
        {
            IClocks clocks = new NerdyClock(new LuxoftInputOutputFormatter());
            clocks.UpdateState(aTime);
            return clocks.GetGraffiti();
        }
    }
}
