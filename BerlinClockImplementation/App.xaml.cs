﻿using BerlinClockImplementation.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace BerlinClockImplementation
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {

            IClocks clocks = new NerdyClock(new LuxoftInputOutputFormatter());
            clocks.UpdateState("00:00:00");
            string result = clocks.GetGraffiti();
        }
    }
}
