﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BerlinClockImplementation
{
    public enum BulbColorStates
    {
        Yellow = 0,
        Red  = 1,
        NoColor = 2,
    }

    public enum ClockRowTypes
    {
        Top = 0,
        HoursByFive = 1,
        HoursByOne = 2,
        MinutesByFive = 3,
        MinutesByOne = 4,
    }

    public enum TimeParts
    {
        Hours = 0,
        Minutes = 1,
        Seconds = 2,
    }
}
