﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BerlinClockImplementation
{
    public class LuxoftInputOutputFormatter
    {
        private IDictionary<BulbColorStates, string> formatterMappings = new Dictionary<BulbColorStates, string>();
        private IDictionary<TimeParts, int> timePartsIndexMappings = new Dictionary<TimeParts, int>();

        private char timePartSeparator = ':';


        public LuxoftInputOutputFormatter()
        {
            formatterMappings.Add(BulbColorStates.Yellow, "Y");
            formatterMappings.Add(BulbColorStates.Red, "R");
            formatterMappings.Add(BulbColorStates.NoColor, "O");
            timePartsIndexMappings.Add(TimeParts.Hours, 0);
            timePartsIndexMappings.Add(TimeParts.Minutes, 1);
            timePartsIndexMappings.Add(TimeParts.Seconds, 2);
        }

        public string GetFormattedValueForColorState(BulbColorStates state)
        {
            return formatterMappings[state] ?? "_";
        }

        public char GetTimePartSeparator()
        {
            return timePartSeparator;
        }

        public int GetTimePartStringIndex(TimeParts timePart)
        {
            return timePartsIndexMappings[timePart];
        }
    }
}
