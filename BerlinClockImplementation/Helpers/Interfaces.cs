﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BerlinClockImplementation
{
    public interface IGraffiti
    {
        string GetGraffiti();
    }

    public interface ITimeUpdatable
    {
        void UpdateState(string newTime);
    }

    public interface IClocks : IGraffiti, ITimeUpdatable
    {

    }

    public interface IRow : IGraffiti, ITimeUpdatable
    {
        
    }
}
