﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BerlinClockImplementation.Model
{
    public class DisplayRowConstructor
    {
        private LuxoftInputOutputFormatter _f;

        public DisplayRowConstructor(LuxoftInputOutputFormatter formatter)
        {
            _f = formatter;
        }

        public DisplayRow MakeRow(ClockRowTypes rowType)
        {
            switch (rowType)
            {
                case ClockRowTypes.Top:
                    return new DisplayRow(_f, rowType, TimeParts.Seconds, 2, new[]
                    {
                        BulbColorStates.Yellow
                    });

                case ClockRowTypes.HoursByFive:
                    return new DisplayRow(_f, rowType, TimeParts.Hours, 5, new[]
                    {
                        BulbColorStates.Red,
                        BulbColorStates.Red,
                        BulbColorStates.Red,
                        BulbColorStates.Red
                    });

                case ClockRowTypes.HoursByOne:
                    return new DisplayRow(_f, rowType, TimeParts.Hours, 1, new[]
                    {
                        BulbColorStates.Red,
                        BulbColorStates.Red,
                        BulbColorStates.Red,
                        BulbColorStates.Red
                    });

                case ClockRowTypes.MinutesByFive:
                    return new DisplayRow(_f, rowType, TimeParts.Minutes, 5, new[]
                    {
                        BulbColorStates.Yellow,
                        BulbColorStates.Yellow,
                        BulbColorStates.Red,
                        BulbColorStates.Yellow,
                        BulbColorStates.Yellow,
                        BulbColorStates.Red,
                        BulbColorStates.Yellow,
                        BulbColorStates.Yellow,
                        BulbColorStates.Red,
                        BulbColorStates.Yellow,
                        BulbColorStates.Yellow
                    });

                case ClockRowTypes.MinutesByOne:
                    return new DisplayRow(_f, rowType, TimeParts.Minutes, 1, new[]
                    {
                        BulbColorStates.Yellow,
                        BulbColorStates.Yellow,
                        BulbColorStates.Yellow,
                        BulbColorStates.Yellow
                    });

                default:
                    return null;
            }
        }
    }

    public class DisplayRow : IRow
    {
        LuxoftInputOutputFormatter _f;
        private ObservableCollection<Bulb> _rowBulbs;
        private int _multiplier;
        ClockRowTypes _clockRowType;
        TimeParts _timePart;

        public DisplayRow(LuxoftInputOutputFormatter formatter, ClockRowTypes clockRowType, TimeParts timePart, int multiplier, BulbColorStates[] rowPalette)
        {
            _f = formatter;
            _clockRowType = clockRowType;
            _timePart = timePart;
            _rowBulbs = new ObservableCollection<Bulb>();
            foreach(var element in rowPalette)
            {
                _rowBulbs.Add(new Bulb(_f, element));
            }
            _multiplier = multiplier;
        }

        public string GetGraffiti()
        {
            string output = "";
            foreach (Bulb bulb in _rowBulbs)
            {
                output += bulb.GetGraffiti();
            }
            return output;
        }

        public void UpdateState(string newTime)
        {
            string valueInScope = newTime.Split(_f.GetTimePartSeparator())[_f.GetTimePartStringIndex(_timePart)];
            int valueInScopeInt = Convert.ToInt32(valueInScope);

            foreach (Bulb bulbie in _rowBulbs)
            {
                switch (_clockRowType)
                {
                    case ClockRowTypes.Top:
                        bulbie.Toggle(valueInScopeInt % _multiplier == 0);
                        break;

                    case ClockRowTypes.HoursByFive:
                        bulbie.Toggle(((valueInScopeInt - (valueInScopeInt % _multiplier)) / _multiplier) >= (_rowBulbs.IndexOf(bulbie) + 1));
                        break;

                    case ClockRowTypes.MinutesByFive:
                        bulbie.Toggle(((valueInScopeInt - (valueInScopeInt % _multiplier)) / _multiplier) >= (_rowBulbs.IndexOf(bulbie) + 1));
                        break;

                    case ClockRowTypes.HoursByOne:
                        bulbie.Toggle((valueInScopeInt - (valueInScopeInt - valueInScopeInt % 5)) >= (_rowBulbs.IndexOf(bulbie) + 1));
                        break;

                    case ClockRowTypes.MinutesByOne:
                        bulbie.Toggle((valueInScopeInt - (valueInScopeInt - valueInScopeInt % 5)) >= (_rowBulbs.IndexOf(bulbie) + 1));
                        break;
                }
            }
        }
    }
}
