﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BerlinClockImplementation.Model
{
    public class NerdyClock : IClocks
    {
        Dictionary<ClockRowTypes, DisplayRow> clockConfig;
        private LuxoftInputOutputFormatter _f;

        public NerdyClock(LuxoftInputOutputFormatter formatter)
        {
            _f = formatter;
            ConstructNerdyClock();
        }

        private void ConstructNerdyClock()
        {
            DisplayRowConstructor constructor = new DisplayRowConstructor(_f);

            clockConfig = new Dictionary<ClockRowTypes, DisplayRow>()
            {
                { ClockRowTypes.Top, constructor.MakeRow(ClockRowTypes.Top) },
                { ClockRowTypes.HoursByFive, constructor.MakeRow(ClockRowTypes.HoursByFive) },
                { ClockRowTypes.HoursByOne, constructor.MakeRow(ClockRowTypes.HoursByOne) },
                { ClockRowTypes.MinutesByFive, constructor.MakeRow(ClockRowTypes.MinutesByFive) },
                { ClockRowTypes.MinutesByOne, constructor.MakeRow(ClockRowTypes.MinutesByOne) },
            };
        }

        public void UpdateState(string newTime)
        {
            foreach (KeyValuePair<ClockRowTypes, DisplayRow> configUnit in clockConfig)
            {
                configUnit.Value.UpdateState(newTime);
            }
        }

        public string GetGraffiti()
        {
            StringBuilder sb = new StringBuilder();
            //sb.Append("\"");
            foreach (KeyValuePair<ClockRowTypes, DisplayRow> configUnit in clockConfig)
            {
                sb.AppendLine(configUnit.Value.GetGraffiti());
            }
            return sb.ToString().TrimEnd('\r', '\n');
        }
    }
}
