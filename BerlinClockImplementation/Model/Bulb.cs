﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BerlinClockImplementation
{
    public class Bulb : IGraffiti, INotifyPropertyChanged
    {
        private BulbColorStates _onState;
        private BulbColorStates _offState;
        LuxoftInputOutputFormatter _formatter;

        private bool _isToggled;
        public bool IsToggled
        {
            get { return _isToggled; }
            set { SetField(ref _isToggled, value, "IsToggled"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public Bulb(LuxoftInputOutputFormatter formatter, BulbColorStates onState, BulbColorStates offState = BulbColorStates.NoColor)
        {
            _onState = onState;
            _offState = offState;
            _formatter = formatter;
        }

        private BulbColorStates GetCurrentColorState()
        {
            return _isToggled ? _onState : _offState;
        }

        public string GetGraffiti()
        {
            return _formatter.GetFormattedValueForColorState(GetCurrentColorState());
        }

        public void Toggle(bool newValue)
        {
            IsToggled = newValue;
        }
    }
}
